package kvstore

/**
 * Created by levineugene on 1/6/14.
 */
object Names {

  var counters = Map.empty[String, Long].withDefault(_ => 0L)

  def actorNameOf(name: String) = synchronized {
    val counter = counters(name)
    counters += name -> (counter + 1)
    name + counter
  }

}
