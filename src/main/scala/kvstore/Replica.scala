package kvstore

import akka.actor._
import kvstore.Arbiter._
import scala.concurrent.duration._
import kvstore.Replicator.{Replicate, Replicated}
import kvstore.Persistence.{Persisted, Persist}

import scala.language.postfixOps
import kvstore.Replica.{OperationAck, OperationFailed}
import kvstore.PersistenceReplicationCoordinator.{PersistedAndReplicated, Replicators}
import akka.event.LoggingReceive

import kvstore.Names._

import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

object Replica {

  sealed trait Operation {
    def key: String

    def id: Long
  }

  case class Insert(key: String, value: String, id: Long) extends Operation

  case class Remove(key: String, id: Long) extends Operation

  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply

  case class OperationAck(id: Long) extends OperationReply

  case class OperationFailed(id: Long) extends OperationReply

  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class PersistenceCoordinator(persistence: ActorRef, persist: Persist, onPersisted: () => Unit) extends Actor {

  var terminated = false

  context.setReceiveTimeout(100 millisecond)

  persistence ! persist

  override def receive = LoggingReceive {
    case ReceiveTimeout ⇒ persistence ! persist
    case Persisted(key, id) =>
      if (!terminated) {
        onPersisted()
        context.stop(self)
        terminated = true
      }
  }
}

object PersistenceReplicationCoordinator {

  case class Replicators(replicators: Set[ActorRef])

  case class PersistedAndReplicated()

}

/**
 *
 * @param replicators replicators
 * @param persistence
 * @param persist
 */
class PersistenceReplicationCoordinator(var replicators: Set[ActorRef], client: Option[ActorRef], operationId: Long,
                                        persistence: ActorRef, persist: Persist) extends Actor with ActorLogging {

  @volatile var persisted = false

  var terminated = false

  // persist
  context.actorOf(Props(classOf[PersistenceCoordinator], persistence, persist, {
    () =>
      persisted = true
      if (isFinished) terminateAck
  }), actorNameOf("PersistenceCoord"))

  // replicate
  for (replicator <- replicators) {
    replicator ! Replicate(persist.key, persist.valueOption, persist.id)
  }

  val timeOutSchedule = context.system.scheduler.scheduleOnce(1 second) {
    self ! ReceiveTimeout
  }

  def isFinished: Boolean = replicators.isEmpty && persisted

  def terminateAck = synchronized {
    if (!terminated) {
      //timeOutSchedule.cancel()
      client.foreach(_ ! OperationAck(operationId))
      context.parent ! PersistedAndReplicated
      context.stop(self)
      terminated = true
    }
  }

  def terminateNack = synchronized {
    if (!terminated) {
      client.foreach(_ ! OperationFailed(operationId))
      context.parent ! PersistedAndReplicated
      context.stop(self)
      terminated = true
    }
  }

  override def receive = LoggingReceive {
    case Replicated(key, id) =>
      replicators -= sender
      if (isFinished) terminateAck
    case Replicators(currentReplicators) =>
      replicators = replicators & currentReplicators
      if (isFinished) terminateAck
    case ReceiveTimeout =>
      if (!isFinished) terminateNack
  }
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with ActorLogging {

  import Replica._
  import Replicator._
  import Persistence._

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var persistAndReplicates = Set.empty[ActorRef]

  val persistence = context.actorOf(persistenceProps, actorNameOf("Persistence"))

  arbiter ! Join

  override def receive = LoggingReceive {
    case JoinedPrimary ⇒ context.become(leader)
    case JoinedSecondary ⇒ context.become(replica)
  }

  def persistAndReplicate(replicators: Set[ActorRef], client: Option[ActorRef], operationId: Long,
                          persistence: ActorRef, persist: Persist) = {
    persistAndReplicates += context.actorOf(Props(classOf[PersistenceReplicationCoordinator], replicators, client, operationId, persistence, persist), actorNameOf("PRC"))
  }

  /* TODO Behavior for  the leader role. */
  val leader = LoggingReceive {
    case Insert(key, value, id) =>
      kv += key -> value
      persistAndReplicate(replicators, Some(sender), id, persistence, Persist(key, Some(value), nextPersistedId))
    //sender ! OperationAck(id)
    case Remove(key, id) =>
      kv -= key
      persistAndReplicate(replicators, Some(sender), id, persistence, Persist(key, None, nextPersistedId))
    //      sender ! OperationAck(id)
    case PersistedAndReplicated =>
      persistAndReplicates -= sender
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Replicas(replicas) =>
      for (terminatedReplica <- secondaries.keySet -- replicas) {
        // terminate
        secondaries.get(terminatedReplica).foreach {
          case replicatorToTerminate =>
            replicatorToTerminate ! PoisonPill
            secondaries -= terminatedReplica
            replicators -= replicatorToTerminate
        }
      }
      var newReplicators = Set[ActorRef]()
      for (newReplica <- replicas -- secondaries.keySet - self) {
        // create
        val newReplicator = context.actorOf(Props(classOf[Replicator], newReplica))
        newReplicators += newReplicator
        replicators += newReplicator
        secondaries += newReplica -> newReplicator
      }
      for ((key, value) <- kv) {
        persistAndReplicate(newReplicators, None, -1, persistence, Persist(key, Some(value), -1))
      }
      persistAndReplicates.foreach {
        _ ! Replicators(replicators)
      }
    case _ =>
  }

  /* TODO Behavior for the replica role. */
  var expectedSeq = 0L
  var persistedId = 0L

  def nextPersistedId: Long = {
    persistedId += 1
    persistedId
  }


  val replica = LoggingReceive {
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Snapshot(key, valueOption, seq) =>
      def updateExpectedSeq() = expectedSeq = Math.max(expectedSeq, seq + 1)
      def persist(persist: Persist, replicator: ActorRef) = {
        context.actorOf(Props(classOf[PersistenceCoordinator], persistence, persist, {
          () => replicator ! SnapshotAck(key, seq)
        }))
      }
      if (seq > expectedSeq) {
        // Ignore
      } else
      if (seq < expectedSeq) {
        sender ! SnapshotAck(key, seq)
        updateExpectedSeq()
      } else {
        valueOption.fold(kv -= key) {
          kv += key -> _
        }
        persist(Persist(key, valueOption, nextPersistedId), sender)
        updateExpectedSeq()
      }
    case _ =>
  }

}
