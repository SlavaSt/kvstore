package kvstore

import akka.actor.{Props, Actor, ActorRef}
import scala.concurrent.duration._

import scala.language.postfixOps
import akka.event.LoggingReceive
import java.util.concurrent.atomic.AtomicLong

object Replicator {

  case class Replicate(key: String, valueOption: Option[String], id: Long)

  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)

  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {

  import Replicator._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  //var _seqCounter = 0L

  val _seqCounter = new AtomicLong()

  def nextSeq = _seqCounter.getAndIncrement

  context.system.scheduler.schedule(100 millisecond, 100 millisecond) {
    acks.foreach {
      case (seq, (_, replicate)) =>
        replica ! Snapshot(replicate.key, replicate.valueOption, seq)
    }
  }

  /* TODO Behavior for the Replicator. */
  override def receive = LoggingReceive {
    case Replicate(key, valueOption, id) =>
      val seq = nextSeq
      replica ! Snapshot(key, valueOption, seq)
      acks += seq ->(sender, Replicate(key, valueOption, id))
    case SnapshotAck(key, seq) =>
      acks.get(seq).foreach {
        case (sender, replicate) =>
          sender ! Replicated(key, replicate.id)
          acks -= seq
      }

    case _ =>
  }

}
